# -*- coding: utf-8 -*-
from odoo.exceptions import Warning

from odoo import models, fields, api
import requests

class Contacto(models.Model):
    _inherit = 'res.partner'
    #@api.onchange('l10n_pe_document_number')
    @api.multi
    def autocomplete_data(self):
        type_doc = self.l10n_pe_document_type
        doc_number = self.l10n_pe_document_number
        if type_doc != False and doc_number != False:
            #type_doc = int(type_doc)
            #raise Warning((type_doc))
            doc = False
            doc_number = str(doc_number)
            if type_doc == '1':
                doc = "dni"
            if type_doc == '6':
                doc = "ruc"
            if doc != False:
                url = "https://apiperu.dev/api/"+doc+"/"+doc_number
                payload = ""
                headers = {
                  'Content-Type': "application/json",
                  'Authorization': "Bearer 0dbeba144aea0fd77d8da07f8294985ce38e59612939af76d924ff9aaaead4c4",
                }
                response = requests.request("GET", url, data=payload, headers=headers)
                response=response.json()
                if type_doc == '1':
                    if 'data' in response:
                        self.name = str(response['data']['nombre_completo'])
                        self.state_id = self.env['res.country.state'].search([('code', '=', 'PE15')])
                        self.l10n_pe_province_id = self.env['l10n_pe.res.country.province'].search([('code', '=', 'PE1501')])
                        self.l10n_pe_district_id = self.env['l10n_pe.res.country.district'].search([('code', '=', 'PE150101')])
                    else:
                        raise Warning((str(response['message'])))

                if type_doc == '6':
                    if 'data' in response:
                        self.company_type = 'company'
                        self.name = str(response['data']['nombre_o_razon_social'])
                        departamento , provincia , distrito = response['data']['ubigeo']
                        #raise Warning((departamento))
                        self.state_id = self.env['res.country.state'].search([('code', '=', 'PE'+str(departamento))])
                        self.l10n_pe_province_id = self.env['l10n_pe.res.country.province'].search([('code', '=', 'PE'+str(provincia))])
                        self.l10n_pe_district_id = self.env['l10n_pe.res.country.district'].search([('code', '=', 'PE'+str(distrito))])
                        self.street = str(response['data']['direccion'])
                        self.l10n_pe_legal_name = str(response['data']['nombre_o_razon_social'])
                        self.l10n_pe_tradename = str(response['data']['nombre_o_razon_social'])
                        self.l10n_pe_sunat_type = str(response['data']['condicion'])
                        self.l10n_pe_sunat_state = str(response['data']['estado'])
                    else:
                        raise Warning((str(response['message'])))
                #x=response['data']['nombre_completo']
                #raise Warning((x))
    @api.onchange('l10n_pe_document_number')
    def _change_document(self):
        self.autocomplete_data()
    @api.onchange('l10n_pe_document_type')
    def _change_document2(self):
        self.autocomplete_data()



